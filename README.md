Tutorial about basics of php/mysql communication

install composer

composer install

install php

www.php.net/downloads.php

install mysql

https://dev.mysql.com/doc/mysql-installation-excerpt/5.7/en/

install packages

composer install

Modify ".env" : Change following line to correspond to your database settings.

DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name
For example
DATABASE_URL=mysql://root:qwerty@127.0.0.1:3306/flytexto_database

Create database using command :

If the database doesn't exist yet, run this command :
$ php bin/console doctrine:database:create
$ php bin/console make:migration
$ php bin/console doctrine:migrations:migrate
run with:
php -S 127.0.0.1:8000 -t public